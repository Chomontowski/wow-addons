package controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import service.zip.ZipService;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class SceneOne implements Initializable {

    @FXML
    private Button directoryChooser;


    @FXML
    private Accordion addonAccordion;

    @FXML
    private Button copyButton;

    @FXML
    private AnchorPane addonsViewPane;

    @FXML
    private AnchorPane configPane;

    private File addonsDir;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            if(addonsViewPane != null) {
                Parent addonsScene = FXMLLoader.load(getClass().getResource("/controller/AddonsView.fxml"));
                this.addonsViewPane.getChildren().add(addonsScene);
            }

            if(configPane != null) {
                Parent configScene = FXMLLoader.load(getClass().getResource("/controller/ConfigView.fxml"));
                this.configPane.getChildren().add(configScene);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void copyAddons() {
        ZipService zipService = new ZipService();

        zipService.zipDirectory(addonsDir, new File("zippedAddons.zip"));
    }






}
