package controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class ConfigView implements Initializable {
    @FXML
    private AnchorPane configPane;

    @FXML
    private ScrollPane configScrollPane;

    @FXML
    private Button addonsSettingsButton;

    @FXML
    private Accordion configAccordion;


    public void initialize(URL location, ResourceBundle resources) {
        String cssFile = getClass().getResource("/css/configView.css").toExternalForm();
        configPane.getStylesheets().add(cssFile);

        configScrollPane.visibleProperty().setValue(false);
    }

    public void setAddonConfigs() {

        /***
         PO krótce
         Metoda ta ma na celu odpalenie folderu z ustawieniami postaci.
         Folder ten znajduje się w folderze :
         World of Warcraft\_retail_\WTF\Account\{MAGIC NUMBER}.
         Pod tym folderem jest informacja o tym jakie serwery są używane. Dla szybszego działania i wczytywania.
         Pod tymi serwerami są postacie, a pod postaciami są konfigi.
         Te konfigi chcemy zczytać. W końcu.. chcemy przekazywać konfiguracje odpowiednich postaci ... Prawda? :)

         Więc jak już mamy folder z magic number, to do akordeona dodajemy X paneli z nazwami serwerów.
         Następnie dla każdego serwera tworzymy akordeon. Pod ten akordeon wpinamy panel dla kazdej postaci.
         Narazie wsio.

         Możesz użyć directyory choosera, albo narazie sobie ustaw stały folder.
         Jak wolisz.
         Zostawiam Ci diretory choosera.
         */

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File("D:\\BattleNet\\World of Warcraft\\_retail_\\WTF\\Account\\133230661#1"));

        File addonsConfigDirectory = directoryChooser.showDialog(this.addonsSettingsButton.getScene().getWindow());
//        File addonsConfigDirectory = new File("D:\\BattleNet\\World of Warcraft\\_retail_\\WTF\\Account\\133230661#1");


        if(!configAccordion.getPanes().isEmpty()) {
            configAccordion.getPanes().removeAll(configAccordion.getPanes());
        }

        Arrays.stream(addonsConfigDirectory.listFiles())
                .filter(File::isDirectory)
                .filter(file -> !file.getName().equalsIgnoreCase("SavedVariables"))
                .forEach(file -> {

                    Accordion serverAccordion = new Accordion();

                    TitledPane serverPane = new TitledPane(file.getName(), serverAccordion);

                    ObservableList<TitledPane> panes = serverAccordion.getPanes();

                    Arrays.stream(file.listFiles()).forEach(characterDir -> {
                        TitledPane characterPane = new TitledPane(characterDir.getName(), null);
                        characterPane.setStyle("-fx-padding: 10px;" +
                                "-fx-start-margin: 10px;" +
                                "-fx-indent: 10px;");
                        panes.add(characterPane);
                    });

                    configAccordion.getPanes().add(serverPane);
                });

        configScrollPane.visibleProperty().setValue(true);

    }
}
