package controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class AddonsView implements Initializable {

    @FXML
    AnchorPane addonsViewAnchorPane;

    @FXML
    private Button addonDirectoryChooser;

    @FXML
    private Accordion addonAccordion;

    @FXML
    private ScrollPane addonScrollPane;

    private File addonsDir;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String cssFile = getClass().getResource("/css/addonsView.css").toExternalForm();
        addonsViewAnchorPane.getStylesheets().add(cssFile);

        addonScrollPane.visibleProperty().setValue(false);
    }


    public void setupAddonsDirectory() {

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File("D:\\BattleNet\\World of Warcraft\\_retail_\\Interface\\AddOns"));

        addonsDir = directoryChooser.showDialog(this.addonDirectoryChooser.getScene().getWindow());


            if(addonsDir != null) {
                listAddons();
                addonScrollPane.visibleProperty().setValue(true);
//                copyButton.setDisable(false);
            }
    }

    private void listAddons() {

        if(!addonAccordion.getPanes().isEmpty()) {
            addonAccordion.getPanes().removeAll(addonAccordion.getPanes());
        }

        if(addonsDir.isDirectory()) {
            Arrays.stream(addonsDir.listFiles()).forEach(file -> {

                Pane pane = new Pane();

                pane.setPrefWidth(addonAccordion.getWidth());
                pane.setPrefHeight(addonAccordion.getHeight());
                pane.getStyleClass().add("-fx-background-color:  black;");

                pane.getChildren().add(new Label(file.getAbsolutePath()));

                TitledPane titledPane = new TitledPane(file.getName(), pane);
                ObservableList<String> classes = titledPane.getStyleClass();

//                classes.add("blue-button");

                addonAccordion.getPanes().add(titledPane);
            });
        }
    }
}
